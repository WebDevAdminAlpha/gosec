package main

import (
	"os"
	"reflect"
	"strings"
	"testing"

	"gitlab.com/gitlab-org/security-products/analyzers/gosec/v2/metadata"
	report "gitlab.com/gitlab-org/security-products/analyzers/report/v2"
	"gitlab.com/gitlab-org/security-products/analyzers/ruleset"
)

func TestConvert(t *testing.T) {
	// Make sure we're backward compatible with the old var name
	levels := []string{
		envVarConfidenceLevel,
		legacyEnvVarConfidenceLevel,
	}
	for _, envLevel := range levels {
		t.Run("With "+envLevel, func(t *testing.T) {
			os.Setenv(envLevel, "3")
			in := `{
				"Issues": [
					{
						"severity": "MEDIUM",
						"confidence": "HIGH",
						"cwe": {
							"ID": "327",
							"URL": "https://cwe.mitre.org/data/definitions/327.html"
						},
						"rule_id": "G501",	
						"details": "Blacklisted import crypto/md5: weak cryptographic primitive",
						"file": "/go/src/app/main.go",
						"code": "\"crypto/md5\"",
						"line": "15"
					},
					{
						"severity": "MEDIUM",
						"confidence": "HIGH",
						"cwe": {
							"ID": "326",
							"URL": "https://cwe.mitre.org/data/definitions/326.html"
						},
						"rule_id": "G401",
						"details": "Use of weak cryptographic primitive",
						"file": "/go/src/app/main.go",
						"code": "md5.New()",
						"line": "11"
					},
					{
						"severity": "LOW",
						"confidence": "LOW",
						"rule_id": "xyz",
						"details": "xyz",
						"file": "/go/src/app/main.go",
						"code": "xyz",
						"line": "1"
					},
					{
						"severity": "LOW",
						"confidence": "HIGH",
						"rule_id": "G105",
						"details": "Use of math/big.Int.Exp function should be audited for modulus == 0",
						"file": "/go/src/app/main.go",
						"code": "z.Exp(x, y, m)",
						"line": "15"
					},
					{
						"severity": "MEDIUM",
						"confidence": "HIGH",
						"cwe": {
							"ID": "118",
							"URL": "https://cwe.mitre.org/data/definitions/811.html"
						},
						"rule_id": "G601",
						"details": "Implicit memory aliasing of items from a range statement",
						"file": "/go/src/app/main.go",
						"code": "code()",
						"line": "13"
					},
					{
                        "severity": "MEDIUM",
                        "confidence": "HIGH",
                        "cwe": {
                                "ID": "200",
                                "URL": "https://cwe.mitre.org/data/definitions/200.html"
                        },
                        "rule_id": "G102",
                        "details": "Binds to all network interfaces",
                        "file": "/go/src/app/xyz/foo.go",
                        "code": "12: \n13: \t_, _ = net.Listen(\n14: \t\t\"tcp\",\n15: \t\t\"0.0.0.0:2000\",\n16: \t)\n17: \n",
                        "line": "13-16",
                        "column": "9"
                	}
				]
			}`

			var scanner = metadata.IssueScanner

			r := strings.NewReader(in)
			want := &report.Report{
				Version: report.CurrentVersion(),
				Vulnerabilities: []report.Vulnerability{
					{
						Category:    report.CategorySast,
						Name:        "Use of a Broken or Risky Cryptographic Algorithm",
						Scanner:     scanner,
						Message:     "Blacklisted import crypto/md5: weak cryptographic primitive",
						Description: "The use of a broken or risky cryptographic algorithm is an unnecessary risk that may result in the exposure of sensitive information.",
						CompareKey:  "app/main.go:15:\"crypto/md5\":CWE-327",
						Severity:    report.SeverityLevelMedium,
						Confidence:  report.ConfidenceLevelHigh,
						Location: report.Location{
							File:      "app/main.go",
							LineStart: 15,
						},
						Identifiers: []report.Identifier{
							{
								Type:  "gosec_rule_id",
								Name:  "Gosec Rule ID G501",
								Value: "G501",
								URL:   "",
							},
							{
								Type:  "CWE",
								Name:  "CWE-327",
								Value: "327",
								URL:   "https://cwe.mitre.org/data/definitions/327.html",
							},
						},
						RawSourceCodeExtract: "\"crypto/md5\"",
					},
					{
						Category:    report.CategorySast,
						Name:        "Inadequate Encryption Strength",
						Scanner:     scanner,
						Message:     "Use of weak cryptographic primitive",
						Description: "The software stores or transmits sensitive data using an encryption scheme that is theoretically sound, but is not strong enough for the level of protection required.",
						CompareKey:  "app/main.go:11:md5.New():CWE-326",
						Severity:    report.SeverityLevelMedium,
						Confidence:  report.ConfidenceLevelHigh,
						Location: report.Location{
							File:      "app/main.go",
							LineStart: 11,
						},
						Identifiers: []report.Identifier{
							{
								Type:  "gosec_rule_id",
								Name:  "Gosec Rule ID G401",
								Value: "G401",
								URL:   "",
							},
							{
								Type:  "CWE",
								Name:  "CWE-326",
								Value: "326",
								URL:   "https://cwe.mitre.org/data/definitions/326.html",
							},
						},
						RawSourceCodeExtract: "md5.New()",
					},
					{
						Category:    report.CategorySast,
						Name:        "Gosec Rule G105",
						Scanner:     scanner,
						Message:     "Use of math/big.Int.Exp function should be audited for modulus == 0",
						Description: "Use of math/big.Int.Exp function should be audited for modulus == 0",
						CompareKey:  "app/main.go:15:z.Exp(x, y, m):G105",
						Severity:    report.SeverityLevelLow,
						Confidence:  report.ConfidenceLevelHigh,
						Location: report.Location{
							File:      "app/main.go",
							LineStart: 15,
						},
						Identifiers: []report.Identifier{
							{
								Type:  "gosec_rule_id",
								Name:  "Gosec Rule ID G105",
								Value: "G105",
								URL:   "",
							},
						},
						RawSourceCodeExtract: "z.Exp(x, y, m)",
					},
					{
						Category:    report.CategorySast,
						Name:        "Incorrect Access of Indexable Resource ('Range Error')",
						Scanner:     scanner,
						Message:     "Implicit memory aliasing of items from a range statement",
						Description: "The software does not restrict or incorrectly restricts operations within the boundaries of a resource that is accessed using an index or pointer, such as memory or files.",
						CompareKey:  "app/main.go:13:code():CWE-118",
						Severity:    report.SeverityLevelMedium,
						Confidence:  report.ConfidenceLevelHigh,
						Location: report.Location{
							File:      "app/main.go",
							LineStart: 13,
						},
						Identifiers: []report.Identifier{
							{
								Type:  "gosec_rule_id",
								Name:  "Gosec Rule ID G601",
								Value: "G601",
								URL:   "",
							},
							{
								Type:  "CWE",
								Name:  "CWE-118",
								Value: "118",
								URL:   "https://cwe.mitre.org/data/definitions/118.html",
							},
						},
						RawSourceCodeExtract: "code()",
					},
					{
						Category:    report.CategorySast,
						Name:        "Information Exposure",
						Scanner:     scanner,
						Message:     "Binds to all network interfaces",
						Description: "An information exposure is the intentional or unintentional disclosure of information to an actor that is not explicitly authorized to have access to that information.",
						CompareKey:  "app/xyz/foo.go:13-16:12: \n13: \t_, _ = net.Listen(\n14: \t\t\"tcp\",\n15: \t\t\"0.0.0.0:2000\",\n16: \t)\n17: \n:CWE-200",
						Severity:    report.SeverityLevelMedium,
						Confidence:  report.ConfidenceLevelHigh,
						Location: report.Location{
							File:      "app/xyz/foo.go",
							LineStart: 13,
							LineEnd:   16,
						},
						Identifiers: []report.Identifier{
							{
								Type:  "gosec_rule_id",
								Name:  "Gosec Rule ID G102",
								Value: "G102",
								URL:   "https://securego.io/docs/rules/g102.html",
							},
							{
								Type:  "CWE",
								Name:  "CWE-200",
								Value: "200",
								URL:   "https://cwe.mitre.org/data/definitions/200.html",
							},
						},
						RawSourceCodeExtract: "12: \n13: \t_, _ = net.Listen(\n14: \t\t\"tcp\",\n15: \t\t\"0.0.0.0:2000\",\n16: \t)\n17: \n",
					},
				},
				DependencyFiles: []report.DependencyFile{},
				Remediations:    []report.Remediation{},
				Analyzer:        "gosec",
				Config:          ruleset.Config{Path: ruleset.PathSAST},
			}
			got, err := convert(r, "app")
			if err != nil {
				t.Fatal(err)
			}
			if !reflect.DeepEqual(want, got) {
				t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
			}

		})
	}
}
